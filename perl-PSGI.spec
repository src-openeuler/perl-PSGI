Name:                perl-PSGI
Version:             1.102
Release:             3
License:             CC-BY-SA-4.0
Summary:             Perl Web Server Gateway Interface Specification
Source0:             https://cpan.metacpan.org/authors/id/M/MI/MIYAGAWA/PSGI-%{version}.tar.gz
URL:                 https://metacpan.org/release/PSGI
BuildArch:           noarch
BuildRequires:       coreutils make perl-interpreter perl-generators perl(inc::Module::Install)
BuildRequires:       perl(Module::Install::Metadata) perl(Module::Install::Repository)
BuildRequires:       perl(Module::Install::WriteAll) sed
%{?perl_default_filter}

%description
This document specifies a standard interface between web servers and Perl web
applications or frameworks, to promote web application portability and reduce
the duplicated efforts by web application framework developers.

%prep
%setup -q -n PSGI-%{version}
rm -r ./inc/*
sed -i -e '/^inc\//d' MANIFEST

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes
%{perl_vendorlib}/*
%{_mandir}/man3/*.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.102-3
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jul 14 2022 Chenyx <chenyixiong3@huawei.com> - 1.102-2
- License compliance rectification

* Fri May 14 2021 zhangtao <zhangtao307@huawei.com> - 1.102-1
- package init
